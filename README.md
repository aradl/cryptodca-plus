# CryptoDCA-plus

Hodl on tight. A cryptocurrency dollar-cost averaging (DCA) script with a few strategy options. The goal of this DCA strategy is to optimize the trade interval according to your portfolio and monthly spending limits. We are able to buy as often as possible because the amount of transactions doesn't affect cost (fees are percent based). We also want to be smart; at each trading opportunity, we assess the market conditions and buy a little more or a little less.

## Setup

1. Install Python, at least version 3.8
1. Install required packages with `pip install -r ./requierments.txt`
1. Setup API and configuration files (see below)
1. Run with `python main.py`, to automate see below 


### API File Setup
For now only coinbase pro is supported. Create an API key from <https://pro.coinbase.com/profile/api> to give the script access to make trades. I recommend only allowing "Trade" and "View". This means you will need to perform bank transfers manually once a month. If I feel brave I may automate this part.
1. Copy the sample file with `cp src/utils/cbpro_api.json.sample src/utils/cbpro_api.json` to create your own copy.
1. Make sure you copy each part of the API key to the api file `cbpro_api.json`, as they will not be shown again.

If you would like status emails I recommend making a free account at <https://www.mailgun.com/>.
1. Copy the sample file with `cp src/utils/mail_api.json.sample src/utils/mail_api.json` to create your own copy.
1. Fill in the required information to `mail_api.json`

### Configuration File Setup
The config file is broken down into a few different sections. `src/hodl.conf` is the only config file. 

- #### Information
	If you decided to setup an email API file then you can get emails every time the script runs and/or once a week with a performance evaluation*.

	*This evaluation currently doesn't know if the transactions on the exchange came from this script or from you. It just calculates the percent gain over all transactions.

- #### Investment Options
	Set the amount that you would like to spend a month to `monthly_amount` and if desired add a max spend amount per trade. This is more of a safety feature.

- #### Portfolio Options
	Set up a desired portfolio following the example provided in the config file and make note that each trading pair has a minimum purchase amount that needs to be filled in. I have not found a way to automate this yet.

- #### Timing
	The trade opportunities can be fixed to once a day at a specified time or occur hourly when the minimum trade time is achieved. (see "What Gets Calculated")

- #### Strategy
	This is the section that gives us a chance to buy a little more or less depending on the current market conditions. Each strategy allows you to set a swing amount which dictates how much more or less you buy depending on the conditions of each strategy. These are additive. The `*_swing` settings are the multipliers which set how much more (a positive value (0.03 -> +3%)) or less (a negative value (-0.1 -> -10%))to buy. There is a `true_swing` for when the conditions are met and a `false_swing` when they are not.
	
	The strategies include:
	- belowMean: Is true when the price is below a mean price (with mean as the 200 day moving average, but is configurable).
	- inDip: Is true when price is in a 'dip'; if the price is below a 200 hour moving average and down 10% in the last 24 hours.
	- mood: Is true when the [Greed and Fear index](https://alternative.me/crypto/fear-and-greed-index/) is between a set of values. (0(fear) -> 100(greed))
	- belowSF: Is true when below the [Stock and Flow model](https://stats.buybitcoinworldwide.com/stock-to-flow/) for BTC only (need to add backtesting)
	- (coming soon) Golden/Death cross 
	- (coming soon) Volatility 
	<br>

- #### Testing
	All settings can be tested over historical data by setting test mode to True and setting the amount of days in the past to start from. This allows insight into how each strategy option works and how good/bad it is without exchange interaction. Automated optimization is coming soon.

#### What Gets Calculated
Some parameters in the portfolio are calculated after runtime. As mentioned previously, the goal is to minimize the interval between trades. For each coin in the portfolio the minimum trade time is calculated based on the scale, monthly investment (30 day rolling window), and the strategy's total minimum swing amount. The timing section will clip this trade time to days or hours.
Each time the script is ran, for each coin it:
- Pulls the last transaction time
- Checks to see if its withing the minimum trade time
- Runs the configured strategies
- Sets a buy order

## Automating
The simplest way to automate this script is with a cronjob on a Linux machine. (Raspberry Pi, old laptop)
To setup a cronjob:
1. Edit a user level cron config with the command `crontab -e`
1. Add a line to describe the scripts current location, python exec, and run frequency (hourly is the lowest supported frequency and is how the example is configured)
	1. For example: `0 *   *    *   *    cd /home/USERNAME/cryptodca-plus/src && /usr/bin/python3 /home/USERNAME/cryptodca-plus/src/main.py`
	1. This site can help setup alternative parameters: <https://crontab.guru/> 

## Donate
Wallets:
- BTC: `bc1q2l5rxc6nvq4s9rrd053j95dlsn090uvrejf0qp`
- SOL: `6a583ZAxWUpjJbe1XQkzJHRbw9jcN4eSAkANcDKK5bwE`

## Contact
I am always open to suggestions and bug fixes.

<aradl.dev@pm.me>

## In Progress
- automatic strategy optimization
- add more strategies

## Future Updates
- support windows
	- Automated execution
- GUI, live plots with performance information
- set external address to transfer to when gas/tx fees are low (hardware wallet, celsius, etc...)
