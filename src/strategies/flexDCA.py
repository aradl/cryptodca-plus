"""
    Flexible DCA 
    
    Comprised of various signals to buy more of less of a coin
    Each one will return a scale based on how much more of less to buy
"""
import numpy as np

class FlexDCA(object):
    def __init__(self, config_dict, exchange, signals):
        self.config_dict = config_dict
        self.exchange    = exchange
        self.signals     = signals

        self.verbose = False

    def calc(self, coin_ticker):
        if(self.verbose): print(f"coin: {coin_ticker}")
        base_scale = 1.0
        for strat, params in self.config_dict.items():
            if(self.verbose): print(f"strategy: {strat}")
            if(self.verbose): print(f"{params}")

            strat_func = getattr(self, strat)
            base_scale += strat_func(params, coin_ticker)

        return base_scale

    def belowMean(self, config, coin_ticker):
        hist_close_price, current_price = self.exchange.get_price_info(
                coin_ticker, 
                config['history_freq'],
                config['history_length'])
        
        # mean historical data
        mean_price = np.mean(hist_close_price)
    
        # set value
        scale = config['false_swing']
        if(current_price < mean_price):
            scale = config['true_swing']

        # debug
        if(self.verbose): print(f"mean: {mean_price}, current: {current_price}, scale: {scale}")
    
        return scale

    def inDip(self, config, coin_ticker):
        hist_close_price, current_price = self.exchange.get_price_info(
                coin_ticker, 
                'hours',
                200)
        
        mean_price = np.mean(hist_close_price)
        short_pchange = current_price - hist_close_price[-23]
        short_pchange = (short_pchange / current_price) * 100

        # below 200 hour average
        scale = config['false_swing']
        if(current_price < mean_price):
            # 10% decrease in 24 hours
            if(short_pchange < -0.10):
                scale = config['true_swing']
        # debug
        if(self.verbose): print(f"mean: {mean_price}, short_pchange: {short_pchange}, current: {current_price}, scale: {scale}")

        return scale

    def belowSF(self, config, coin_ticker):
        if(coin_ticker != 'BTC'): return 0.0 # for now
        if(self.signals.override_date != None): return 0.0 #TODO back test support

        hist_close_price, current_price = self.exchange.get_price_info(
                coin_ticker, 
                'hours',
                10)
        snf_price = self.signals.snf(coin_ticker)

        scale = config['false_swing']
        if(current_price < snf_price):
            scale = config['true_swing']

        # debug
        if(self.verbose): print(f"snf_price: {snf_price}, current: {current_price}, scale: {scale}")

        return scale

    def mood(self, config, coin_ticker):
        #TODO not sure the best action to take
        # need to do some EDA

        fng_index = self.signals.fng()

        scale = config['false_swing']
        # maybe high greed is a bubble?
        # buy more when within range
        if(config['fng_low'] < fng_index < config['fng_high']):
            scale = config['true_swing']

        # debug
        if(self.verbose): print(f"fng_index: {fng_index}, scale: {scale}")

        return scale

    def isVolatile(self):
        pass

