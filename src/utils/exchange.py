# std
import os
import datetime as dt

# pip
import json
import numpy as np
import dateutil.parser
import pandas as pd

import cbpro

# program constants
CBPRO_API_FILE= './cbpro_api.json'
CBPRO = 'cbpro'

class Exchange(object):
    """ General exchange interface to encapsulate working with the trade APIs"""
    def __init__(self, config=None):
        self.name = CBPRO

        if(config is None):
            self.spend_ticker = 'USD'
            self.max_trade_amount = 25.00
        else:
            self.spend_ticker = config.spend_ticker
            self.max_trade_amount = config.investment['max_single_purchase']

        # support for back testing
        self.override_date = None

        # get authenticator
        if(self.name == CBPRO):
            # setup clients
            api_data = self.__get_api_data('cbpro_api.json')
            self.public_client, self.auth_client = self.__get_cbpro_auth(api_data)

            # get spending account information
            accounts = self.auth_client.get_accounts()
            for acc in accounts:
                if(acc['currency'] == self.spend_ticker):
                    self.spend_account_id = acc['id']
                    break

    def __get_api_data(self, api_fn):
        """Read in api information from file"""
        current_path = os.path.dirname(os.path.abspath(__file__))
        api_fp       = os.path.join(current_path, api_fn)
    
        # preform file checks
        if os.path.isfile(api_fp) and api_fp.endswith(".json"):
            with open(api_fp) as data_file:
                api_data = json.load(data_file)            
    
                return api_data
                
        else:
            return None

    def __get_cbpro_auth(self, api_data):
        """Get athenticated cbpro client """
        try:
            # api connect
            auth_client = cbpro.AuthenticatedClient(api_data['api_key'], 
                                                    api_data['api_b64secret'], 
                                                    api_data['api_passphrase'])
        except:
            print('ERROR: Failed to authenticate client')
            auth_client = None

        try:
            # api connect
            public_client = cbpro.PublicClient()

        except:
            print('ERROR: Failed get public client')
            public_client = None

        return public_client, auth_client


    def get_price_info(self, coin, resolution='days', points=200):
        """Get current price information and history from ticker"""
        prod_id = '{}-USD'.format(coin)

        if(resolution == 'days'):
            gran_hours = 24*60*60

        elif(resolution == 'hours'):
            gran_hours = 60*60

        # calculate range
        if(self.override_date is None):
            start_date = None
            end_date   = None
        else:
            end_date    = self.override_date
            end_date_dt = dateutil.parser.isoparse(end_date)

            if(resolution == 'days'):
                start_date_dt = end_date_dt - dt.timedelta(days=points)
            elif(resolution == 'hours'):
                start_date_dt = end_date_dt - dt.timedelta(hours=points)

            start_date    = start_date_dt.isoformat()

        # captures price per 'granularity in seconds' for the last 300 points
        # get history where candel resolution is by day
        hist_price = self.public_client.get_product_historic_rates(
                        prod_id,
                        granularity=gran_hours, 
                        start=start_date,
                        end=end_date)

        if(len(hist_price) == 0):
            print(f"{prod_id} does not have enough history on exchange for date {start_date}")

        # [[time iso 8601, low, high, open, close, volume ], ...]
        hist_price = np.array(hist_price)

        # get recent X points
        hist_close_price = hist_price[:points,4]
    
        # get current price
        if(start_date is None):
            current_info  = self.public_client.get_product_ticker(prod_id)
            current_price = float(current_info['price'])
        else:
            current_price = float(hist_close_price[0])

        return hist_close_price, current_price

    def get_spend_amount(self):
        """Get current amount in spending account"""
        # each currency type has a unique account id
        account_info = self.auth_client.get_account(self.spend_account_id)
        return float(account_info['available'])

    def fills_for(self, coin):
        """Get completed transactions"""
        return self.auth_client.get_fills(product_id="{}-{}".format(coin, self.spend_ticker))

    def last_tx_date_for(self, coin):
        """Get last transaction date for a certain ticker"""
        fills = self.fills_for(coin)
        try:
            last_fill = next(fills)
        except StopIteration:
            return None

        last_filled_date = last_fill['created_at']
        last_date_dt     = dateutil.parser.isoparse(last_filled_date)

        return last_date_dt
    
    def trade_to(self, coin, amount):
        """Set market order for given ticker and price"""
        # do we have enough funds
        if(amount > self.get_spend_amount()):
            return 'ERROR: Need to add {} USD to complete trade'.format(amount - usd_avail)

        # is the amount unusually high
        if(amount > self.max_trade_amount):
            return 'ERROR: Single trade spend amount, {}, exceeded limit of {}'.format(amount, self.max_trade_amount)

        # make the trade
        funds_str   = '{:.2f}'.format(amount)
        return self.auth_client.place_market_order(product_id='{}-{}'.format(coin, self.spend_ticker), 
                                   side='buy', 
                                   funds=funds_str)

    def get_account_transactions(self, coin):
        """Get account transactions for given ticker"""
        #TODO find a way to get fills within date range
        product_id = "{}-{}".format(coin, self.spend_ticker)
        gen_fills  = self.auth_client.get_fills(product_id=product_id)

        d = {'date': [],
             'price': [],
             'size': [],}
        for fill in gen_fills:
            # only fills which have been completed
            # b/c we are hodling we don't sell
            if(fill['settled'] and (fill['side'] == 'buy')):
                d['date'].append(fill['created_at'])
                d['price'].append(float(fill['price']))
                d['size'].append(float(fill['size']))


        df_txs = pd.DataFrame(data=d)
        return df_txs

    def get_account_profile(self):
        #TODO create ascii pie chart for portfolio
        # https://codegolf.stackexchange.com/questions/23350/ascii-art-pie-chart

        return 'ToDo'

    def build_txs_portfolio(self, portfolio):
        """Get transactions for all coins in portfolio"""
        for coin in portfolio:
            coin['txs'] = self.get_account_transactions(coin['ticker'])

        return portfolio


    def calculate_performance(self, txs_portfolio):
        """Calculate percent gain for portfolio"""
        #NOTE some assumptions are made here
        #       - we are not selling
        #       - when coins are transfered we still own them
        #       - does not differentiate between DCA buys and other buys ...yet
        #           - could do some time corrilation to filter

        for coin in txs_portfolio:
            # calculate amount spend per coin
            coin['txs']['cost'] = coin['txs']['size'] * coin['txs']['price']

            # find current price
            _, current_price = self.get_price_info(coin['ticker'])

            # calculate totals
            total_bought_during_tx = coin['txs']['size'].sum()
            total_spent_during_tx  = coin['txs']['cost'].sum()

            # calculate performance
            coin['gain'] = (((total_bought_during_tx * current_price) - total_spent_during_tx) / 
                                total_spent_during_tx) * 100
            coin['gain'] = np.around(coin['gain'], 2)
            
        return txs_portfolio

    def plot_performance(self, perform_portfolio):
        # TODO
        # plot price history, with buy time and amount
        pass

def main():

    print('Test Account Information')
    ex = Exchange()

    print('\nspend id for {}'.format(ex.spend_ticker))
    print(ex.spend_account_id)

    print('\nprice info for BTC')
    print(ex.get_price_info('BTC'))

    print('\n{} available in account {}'.format(ex.get_spend_amount(), ex.spend_ticker))

    print('\nrecent trades for BTC')
    print(ex.fills_for('BTC'))

    print('\nlast trade for BTC')
    print(ex.last_tx_date_for('BTC'))

    print('\naccount transactions')
    print(ex.get_account_transactions('BTC'))

    print('\nbuilt transactions')
    sample_portfolio = [
	                 {'ticker': 'BTC',   'scale': 0.00, 'usd_min': 5.00},
	                 {'ticker': 'ADA',   'scale': 0.00, 'usd_min': 5.00},
	                 {'ticker': 'ETH',   'scale': 0.00, 'usd_min': 5.00},
	                 {'ticker': 'SOL',   'scale': 0.00, 'usd_min': 5.00},
	                ]
    txs_portfolio = ex.build_txs_portfolio(sample_portfolio)
    print(txs_portfolio)

    print('\ncalculate performance')
    portfolio = ex.calculate_performance(txs_portfolio)
    for coin in portfolio:
        print(f"{coin['ticker']}: {coin['gain']}%")

    #WARNING this will make a buy on your account
    if(False):
        val = input('\ntest trade for $1 YFI?\n (YFI has a small min purchase price)\n (y / n (default)) >')
        if(val == 'y'):
            ret = ex.trade_to('YFI', 1.00)
            print(ret)

# run the main function
if __name__ == '__main__':
    main()

