# std
import os
import sys
import datetime as dt
import math

# pip
import json
import numpy as np
import dateutil.parser
import pandas as pd
import requests

class Signals(object):
    """ Interface to encapsulate crypto trading signals"""
    def __init__(self, config=None):
        self.cached_fng = None

        # support for backtesting
        self.override_date = None

    def fng(self, ticker=None):
        # https://alternative.me/crypto/fear-and-greed-index/#api


        if(self.override_date is None):
            amt     = 1
            fng_url = f"https://api.alternative.me/fng/?limit={amt}&date_format=us"

            response = requests.get(fng_url)
            res_dict = response.json()
            fng_list = res_dict['data']

            fng = float(fng_list[0]['value'])

        else:
            if(self.cached_fng is None):
                amt     = 0
                fng_url = f"https://api.alternative.me/fng/?limit={amt}&date_format=us"

                response = requests.get(fng_url)
                res_dict = response.json()

                fng_list = res_dict['data']


                self.cached_fng = {}
                for item in fng_list:
                    self.cached_fng[item['timestamp']] = float(item['value'])

            # figure out date mess
            date_dt  = dateutil.parser.isoparse(self.override_date)
            date_str = date_dt.strftime("%m-%d-%Y")
            fng      = self.cached_fng[date_str]


        return fng

    def snf(self, ticker):
        # reference: https://stats.buybitcoinworldwide.com/stock-to-flow/
        # data reference: https://messari.io/
        #TODO find price model for other coins
        if(self.override_date is None): 
            snf_url = "https://data.messari.io/api/v1/assets/bitcoin/metrics"
            response = requests.get(snf_url)
            res_dict = response.json()

            SF = res_dict['data']['supply']['stock_to_flow']
        else:
            SF = np.nan
            """
            #TODO get by date for backtesting
            asset_key  = ticker
            metric_id  = 'stock-to-flow'
            metric_id  = 'supply'
            start_date = 2020/10/10#date # year/month/day
            end_date   = start_date
            snf_url = f"https://data.messari.io/api/v1/assets/{asset_key}/metrics/{metric_id}/time-series?start={start_date}&end={end_date}&interval=1d"

            response = requests.get(snf_url)
            res_dict = response.json()
            print(res_dict)

            SF = res_dict['data']['supply']['stock_to_flow']
            """

        # SF = stock / flow
        # Model price (USD) = exp(-1.84) * SF ^ 3.36
        model_price = math.exp(-1.84) * (SF ** 3.36)
        model_price = np.around(model_price, 2)

        return model_price 

    def addr_count(self, ticker):
        #TODO check out https://docs.coinmetrics.io/asset-metrics/asset-metrics-overview
        #https://coinmetrics.io/community-network-data/
        pass

    def dng_cross(self, ticker):
        pass

def main():

    print('Test Signal Capture')
    sig    = Signals()
    date   = '2021-07-11T21:23:27.379632+00:00'
    ticker = 'BTC'

    print(f"\n Current fng index: {sig.fng(ticker)}")
    print(f"\n Current snf price: {sig.snf(ticker)}")

    sig.override_date = date
    print(f"\n Past fng index for ({date}): {sig.fng(ticker)}")



# run the main function
if __name__ == '__main__':
    main()
