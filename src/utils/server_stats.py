import os

from send_mail import send_simple_message 

def get_ips():
    # get local and global IP
    from requests import get
    import socket

    global_ip = get('https://api.ipify.org').text

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    local_ip  = s.getsockname()[0]
    s.close()

    return 'Local IP: {}\n Global IP: {}'.format(local_ip, global_ip)

def get_uptime():
    # get server uptime and load
    try:
        f = open( "/proc/uptime" )
        contents = f.read().split()
        f.close()
    except:
        return "Cannot open uptime file: /proc/uptime"

    total_seconds = float(contents[0])

    # Helper vars:
    MINUTE  = 60
    HOUR    = MINUTE * 60
    DAY     = HOUR * 24

    # Get the days, hours, etc:
    days    = int( total_seconds / DAY )
    hours   = int( ( total_seconds % DAY ) / HOUR )
    minutes = int( ( total_seconds % HOUR ) / MINUTE )
    seconds = int( total_seconds % MINUTE )

    # Build up the pretty string 
    # (like this: "N days, N hours, N minutes, N seconds")
    string = ""
    if days > 0:
        string += str(days) + " " + (days == 1 and "day" or "days" ) + ", "
    if len(string) > 0 or hours > 0:
        string += str(hours) + " " + (hours == 1 and "hour" or "hours" ) + ", "
    if len(string) > 0 or minutes > 0:
        string += str(minutes) + " " + (minutes == 1 and "minute" or "minutes" ) + ", "
    string += str(seconds) + " " + (seconds == 1 and "second" or "seconds" )

    return 'Uptime: {}\n Load: {}'.format(string, os.getloadavg())

def gather_stats():
    # send stats via email/text
    stats = '\n'
    stats += get_ips() + '\n'
    stats += get_uptime() + '\n'

    return stats

def main():
    stats = gather_stats()
    send_simple_message('Server Stats', stats)

# run the main function
if __name__ == '__main__':
    main()

