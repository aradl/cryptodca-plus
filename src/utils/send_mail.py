import os

import requests
import json

# program constants
MAIL_API_FILE= "./mail_api.json"

# function to send an email using a mail api (ex. mailgun)
def send_simple_message(subject, content):
    current_path = os.path.dirname(os.path.abspath(__file__))
    api_fp       = os.path.join(current_path, MAIL_API_FILE)

    # preform file checks
    if os.path.isfile(api_fp) and api_fp.endswith(".json"):
        with open(api_fp) as data_file:
            api_data = json.load(data_file)            
            
            # issue an api request
            return requests.post(
                api_data['api_url'],
                auth=("api", api_data['api_key']),
                data={"from": api_data['api_send_address'],
                    "to": [api_data['to_email']],
                    "subject": subject,
                    "text": content})
    else:
        return "ERROR: No such api file found"


