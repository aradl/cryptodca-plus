#!/usr/bin/python3
"""
    General buy and hold with some extra options
"""
# std
import sys
import time
import datetime as dt
import calendar

# pip
import numpy as np
import pandas as pd
import dateutil.parser
import configargparse
import yaml

# own
from strategies.flexDCA import FlexDCA
from utils.send_mail import send_simple_message 
from utils.exchange import Exchange
from utils.signals import Signals

def parse_args():
    """Setup and read in arguments for script"""
    p = configargparse.ArgParser(
                default_config_files=['./hodl.conf'],
                config_file_parser_class=configargparse.ConfigparserConfigFileParser,
            )

    # [Information]
    p.add('--runtime-email', default=False, action='store_true', 
           help='Send email after every run during trade hours')
    p.add('--weekly-performance-email', default='Sunday', type=str, 
           help='What day to send the performance email')

    # [Exchange]
    p.add('--spend-ticker', default='USD', type=str, 
           help='What account to use for trades i.e (USD, USDC, USDT)')

    # [Portfolio]
    p.add('--investment', type=yaml.safe_load) 
    p.add('--portfolio', type=yaml.safe_load, action='append') 

    # [timing]
    p.add('--time-to-trade', default='03:00:00', type=str, 
           help='Time to submit orders to exchange')
    p.add('--trade-frequency', default='days', type=str, 
           help='How often to check for trading opportunities')

    # [strategy]
    p.add('--flexdca-config', type=yaml.safe_load) 

    # [testing]
    p.add('--test-mode', default=False, action='store_true', 
            help='Enable test mode for backtesting strategy settings')
    p.add('--test-duration-days', default=7, type=int, 
            help='Amount of days to backtest strategies starting from today')

    # parse
    return p.parse_args()

def setup_args(args):
    """Derive infered arguments and check for issues"""

    # calculate max swing
    true_swing = 0
    false_swing  = 0
    for key, val in args.flexdca_config.items():
        true_swing += val['true_swing'] 
        false_swing  += val['false_swing']

    args.investment['max_swing'] = (false_swing, true_swing)

    scale = 0
    for coin in args.portfolio:
        # check that all scales equal 1.0
        scale += coin['scale']

        # calculate investment period per coin
        base_amt = coin['usd_min'] / \
                      (1.0 + args.investment['max_swing'][0])
        base_amt = np.around(base_amt, 2)

        # days within a month
        frequency_days = (coin['scale'] * \
                args.investment['monthly_amount']) / base_amt

        # day frequency w.r.t a month
        frequency_days  = 30 / np.floor(frequency_days)
        frequency_hours  = frequency_days * 24
        frequency_hours  = np.ceil(frequency_hours)

        if(args.trade_frequency == 'days'):
            frequency_hours = np.ceil(frequency_days) * 24

        coin['base_amount']      = base_amt
        coin['trade_freq_hours'] = frequency_hours

    if(scale != 1.0):
        print(f'WARNING: scale does not equal 1.0, is {scale}')

    return args

def is_within_time(time, tol_min=5):
    """Check the time argument against the current time
    
        time: time provided as a string in easy to read format
        tol_min: amount of tolerance around current time to let pass
    """
    time_run = dt.datetime.strptime(time, '%H:%M:%S').time()

    date_now = dt.datetime.now()
    time_now = date_now.time()
    time_tol = dt.timedelta(minutes=tol_min)

    time_low  = (dt.datetime.combine(dt.date(1,1,1), time_run) - time_tol).time()
    time_high = (dt.datetime.combine(dt.date(1,1,1), time_run) + time_tol).time()

    is_time = False
    if(time_low < time_now < time_high):
        is_time = True

    return is_time 

def avail_for_trade(last_tx_date, trade_freq_hours, 
                        date_now=None):
    """Determine if its time to trade given the last transaction 
        date and trade frequency
    
        last_tx_date: time in datetime object
        trade_freq_hours: amount of hours between trade
        date_now: allows for overriding date to support backtesting
    """

    trade_tol_hours = 0.5
    if(last_tx_date is None):
        diff_hours = trade_freq_hours
    else:
        # find difference
        if(date_now is None):
            date_now  = dt.datetime.now(dt.timezone.utc)
        date_diff = date_now - last_tx_date

        diff_days, diff_seconds = date_diff.days, date_diff.seconds
        diff_hours = (diff_days * 24) + (diff_seconds // 3600)

    # are we greater than the trade frequency window
    is_time = False
    if((trade_freq_hours-trade_tol_hours) <= diff_hours):
        is_time = True

    return is_time, diff_hours

def buy_coin(exchange, coin_dict, amount):
    """Send buy order to exchange"""
    rpt = 'Set market buy for {} with {} usd...\n'.format(
            coin_dict['ticker'], amount)

    # buy
    ret  = exchange.trade_to(coin_dict['ticker'], amount)
    rpt += '\n{}\n'.format(ret)

    return rpt

def gen_performance_report(per_portfolio):
    """Build string to display performance information"""
    rpt = "\n"
    rpt += "Over the past ~100 transactions:\n"
    for coin in per_portfolio:
        rpt += f"{coin['ticker']}: {coin['gain']}%\n"

    return rpt

def calc_backtest_date(hour):
    """Calculate iso time from now - day"""
    date_now   = dt.datetime.now(dt.timezone.utc)
    time_delta = dt.timedelta(hours=hour)
    date_old   = date_now - time_delta
    iso_date   = date_old.isoformat()

    return iso_date

def backtest(config):
    """Run test on historic data and calculate performance"""
    # setup and connect to exchange
    exchange = Exchange(config)
    signals  = Signals(config)

    # init tx capture
    for coin in config.portfolio:
        coin['txs'] = {'date': [],
                       'price': [],
                       'size': [],}

    # calculate test range
    if(config.trade_frequency == 'days'):
        test_range = range(config.test_duration_days*24, -1, -24)
    elif(config.trade_frequency == 'hours'):
        test_range = range(config.test_duration_days*24, -1, -1)

    print('Starting backtest...')
    for hour in test_range:
        exchange.override_date = calc_backtest_date(hour)
        signals.override_date  = exchange.override_date
        dca = FlexDCA(config.flexdca_config, exchange, signals)
        print(f'day: -{hour/24}, timestamp: {exchange.override_date}')

        # loop through coins 
        for coin in config.portfolio:
            # are we far enough from last trade
            if(len(coin['txs']['date']) == 0):
                dt_tx_date   = None
            else:
                last_tx_date = coin['txs']['date'][-1]
                dt_tx_date   = dateutil.parser.isoparse(
                                    last_tx_date)
            trade_freq_hours = coin['trade_freq_hours']
            dt_last_date     = dateutil.parser.isoparse(
                                    exchange.override_date)

            avail, _ = avail_for_trade(dt_tx_date, 
                                       trade_freq_hours,
                                       dt_last_date)
    
            if(avail):
                # gather price information
                _, current_price = exchange.get_price_info(coin['ticker'])

                # calculate investment
                fdca_scale = dca.calc(coin['ticker'])
                buy_amount = fdca_scale * coin['base_amount']
                buy_amount = np.around(buy_amount, 2)

                # record action
                coin['txs']['date'].append(exchange.override_date)
                coin['txs']['price'].append(current_price)
                coin['txs']['size'].append(buy_amount / current_price)
            else:
                continue

        # small delay to not overload api calls
        time.sleep(0.010)

    # convert to dataframe
    for coin in config.portfolio:
        coin['txs'] = pd.DataFrame(data=coin['txs'])

    # calculate performance
    config.portfolio = exchange.calculate_performance(config.portfolio)
    for coin in config.portfolio:
        print(f"{coin['ticker']}: {coin['gain']}%")

def run(config):
    """Run main DCA loop"""
    # time check
    date_now = dt.datetime.now()
    if(config.trade_frequency == 'days'):
        if(not is_within_time(config.time_to_trade)):
            sys.exit()

    # start report
    rpt = ""
    rpt += 'Time: {}\n'.format(date_now)
    rpt += '\nPortfolio:\n{}\n'.format(config.portfolio)

    # setup and connect to exchange
    exchange = Exchange(config)
    signals  = Signals(config)
    dca      = FlexDCA(config.flexdca_config, exchange, signals)

    # loop through coins 
    for coin in config.portfolio:
        rpt += '\nCoin: {}\n'.format(coin['ticker'])

        # gather price information
        _, current_price = exchange.get_price_info(coin['ticker'])
        rpt += 'Price: {}\n'.format(current_price)

        # are we far enough from last trade
        last_tx_date      = exchange.last_tx_date_for(coin['ticker'])
        trade_freq_hours  = coin['trade_freq_hours']
        avail, diff_hours = avail_for_trade(last_tx_date, 
                                trade_freq_hours)
        if(avail):
            # calculate investment
            fdca_scale = dca.calc(coin['ticker'])
            buy_amount = fdca_scale * coin['base_amount']
            buy_amount = np.around(buy_amount, 2)

            # buy
            rpt += buy_coin(exchange, coin, buy_amount)

        else:
            rpt += 'Hours: {:.0f}/{:.0f}\n'.format(diff_hours, trade_freq_hours)
            rpt += 'Outside of trade frequency window\n'
            continue


    # runtime report
    print(rpt)
    if(config.runtime_email):
        ret = send_simple_message('Hodl Report', rpt)
        print(ret)

    # performance report
    day_en = calendar.day_name[dt.date.today().weekday()]
    if(config.weekly_performance_email == day_en):
        if(is_within_time(config.time_to_trade)):
            txs_portfolio = exchange.build_txs_portfolio(config.portfolio)
            per_portfolio = exchange.calculate_performance(txs_portfolio)
            per_rpt       = gen_performance_report(per_portfolio)

            ret = send_simple_message('Performance Report', per_rpt)
            print(ret)

def main():
    # setup
    args   = parse_args()
    config = setup_args(args)

    if(config.test_mode):
        backtest(config)
    else:
        run(config)


# run the main function
if __name__ == '__main__':
    main()

