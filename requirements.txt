requests==2.13.0
cbpro==1.1.4
pandas==1.3.1
numpy==1.20.3
ConfigArgParse==1.4.1
python_dateutil==2.8.2
PyYAML==5.4.1
